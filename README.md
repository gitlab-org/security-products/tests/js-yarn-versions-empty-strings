# js-yarn-versions-empty-strings

This project has a CI pipeline that uploads a predefined Dependency Scanning report where all versions have been set to an empty strings.
(Vulnerability details and solution haven't been changed.)
